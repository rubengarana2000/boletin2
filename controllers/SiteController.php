<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\db\Query;
use yii\db\QueryInterface;

class SiteController extends Controller {
    
    
    public function actionConsultaextra() {
$dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT nombre FROM ciclista  WHERE nomequipo=(SELECT nomequipo FROM ciclista WHERE nombre="Miguel Induraín") AND nombre!="Miguel Induraín"',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta extra con DAO",
                    "enunciado" => "todos los ciclistas del equipo de Miguel Induráin sin incluir a Miguel Induráin (utilizando una subconsulta)",
                    "sql" =>"SELECT nombre FROM ciclista  WHERE nomequipo=(SELECT nomequipo FROM ciclista WHERE nombre='Miguel Induraín') AND nombre!='Miguel Induraín'"
        ]);
        
    }
  
    public function actionConsultaextra1() {
        
        $dataProvider = new ActiveDataProvider([
            'query'=>\app\models\Ciclista::find()
                ->select("nombre")
                ->where("nomequipo=(SELECT nomequipo FROM ciclista WHERE nombre='Miguel Induraín')and nombre!='Miguel Induraín'"),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['nombre'],
                    "titulo" => "Consulta extra con Active Record",
                    "enunciado" => "todos los ciclistas del equipo de Miguel Induráin sin incluir a Miguel Induráin (utilizando una subconsulta)",
                    "sql" => "SELECT nombre FROM ciclista  WHERE nomequipo=(SELECT nomequipo FROM ciclista WHERE nombre='Miguel Induraín') AND nombre!='Miguel Induraín'"
        ]);
    }


    public function actionConsulta1() {
        $dataProvider = new ActiveDataProvider([
            'query'=>\app\models\Ciclista::find()
                ->select("COUNT(*)as 'ciclistas' "),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['ciclistas'],
                    "titulo" => "Consulta 1 con Active Record",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT COUNT(*) AS 'numero de ciclistas' FROM ciclista c;"
        ]);
    }
    public function actionConsulta2(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("COUNT(*) AS num_ciclistas")
                ->where("nomequipo='Banesto'"),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['num_ciclistas'],
                    "titulo" => "Consulta 2 con Active Record",
                    "enunciado" => "Número de ciclistas que hay del equipo Banesto",
                    "sql" => 'SELECT COUNT(*) AS num_ciclistas FROM ciclista WHERE nomequipo="Banesto";'
        ]);
        
    }
    public function actionConsulta3(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("AVG(edad) as edad_media"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['edad_media'],
                    "titulo" => "Consulta 3 con Active Record",
                    "enunciado" => "Edad media de los ciclistas",
                    "sql" => 'SELECT AVG(c.edad)AS "edad media" FROM ciclista c'
        ]);
    }
     public function actionConsulta4(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("AVG(edad) as edad_media")
                ->where("nomequipo='Banesto'"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['edad_media'],
                    "titulo" => "Consulta 4 con Active Record",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => 'SELECT AVG(edad)AS "edad media" FROM ciclista WHERE nomequipo="Banesto";'
        ]);
    }
     public function actionConsulta5(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("AVG(edad) as edad_media,nomequipo")
                ->groupBy("nomequipo"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['edad_media','nomequipo'],
                    "titulo" => "Consulta 5 con Active Record",
                    "enunciado" => "La edad media de los ciclistas por cada equipo",
                    "sql" => 'ELECT AVG(edad) AS "edad media",nomequipo FROM ciclista GROUP BY nomequipo;'

        ]);
    }
     public function actionConsulta6(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("count(*) as num_ciclistas,nomequipo")
            ->groupBy("nomequipo"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['num_ciclistas','nomequipo'],
                    "titulo" => "Consulta 6 con Active Record",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => 'SELECT COUNT(*) AS "numero de ciclistas",c.nomequipo FROM ciclista c GROUP BY c.nomequipo;'
        ]);
    }
     public function actionConsulta7(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Puerto::find()
                ->select("count(*) as num_puertos"),
                
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['num_puertos'],
                    "titulo" => "Consulta 7 con Active Record",
                    "enunciado" => "El número total de puertos",
                    "sql" => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto p;'
        ]);
    }
     public function actionConsulta8(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Puerto::find()
                ->select("count(*) as num_puertos")
                ->where("altura>1500")
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['num_puertos'],
                    "titulo" => "Consulta 8 con Active Record",
                    "enunciado" => "El número total de puertos mayores de 1500",
                    "sql" => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto p WHERE p.altura>1500;'
        ]);
    }
     public function actionConsulta9(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("nomequipo")
                ->groupBy("nomequipo")
                ->having("count(*)>4")
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 9 con Active Record",
                    "enunciado" => " Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql" => 'SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;'
        ]);
    }
     public function actionConsulta10(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Ciclista::find()
                ->select("nomequipo")
                ->where("edad between 28 and 32")
                ->groupBy("nomequipo")
                ->having("count(*)>4")
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 10 con Active Record",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" => 'SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32  GROUP BY c.nomequipo HAVING COUNT(*)>4;'
        ]);
    }
     public function actionConsulta11(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Etapa::find()
                ->select("dorsal,count(dorsal) as etapas_ganadas")
                ->groupBy("dorsal"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['dorsal','etapas_ganadas'],
                    "titulo" => "Consulta 11 con Active Record",
                    "enunciado" =>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" =>'SELECT e.dorsal, COUNT(e.dorsal) AS "etapas ganadas"FROM etapa e GROUP BY e.dorsal;
'
        ]);
    }
     public function actionConsulta12(){
        $dataProvider=new ActiveDataProvider([
            'query'=> \app\models\Etapa::find()
                ->select("dorsal,count(dorsal) as etapas_ganadas")
                ->groupBy("dorsal")
                ->having("count(*)>1"),
            
        ]);
        return $this->render("resultados",[
            "resultados" => $dataProvider,
                    "campos" => ['dorsal','etapas_ganadas'],
                    "titulo" => "Consulta 12 con Active Record",
                    "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" => 'SELECT e.dorsal, COUNT(e.dorsal) AS "etapas ganadas"FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;'
        ]);
    }

    public function actionConsulta1b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) FROM ciclista ',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(*)'],
                    "titulo" => "Consulta 1 con DAO",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT COUNT(*) FROM ciclista c"
        ]);
    }

    public function actionConsulta2b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) FROM ciclista WHERE nomequipo="Banesto"',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(*)'],
                    "titulo" => "consulta 2 dao",
                    "enunciado" => "Número de ciclistas que hay del equipo Banesto",
                    "sql" => 'SELECT COUNT(*) FROM ciclista WHERE nomequipo="Banesto"',
        ]);
    }

    public function actionConsulta3b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => "SELECT AVG(edad) FROM ciclista",
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['AVG(edad)'],
                    "titulo" => "consulta 3 con dao",
                    "enunciado" => "Edad media de los ciclistas",
                    'sql' => 'SELECT AVG(c.edad) FROM ciclista',
        ]);
    }

    public function actionConsulta4b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT AVG(edad) FROM ciclista WHERE nomequipo="Banesto"',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);

        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['AVG(edad)'],
                    "titulo" => "consulta 4 con dao",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => 'SELECT AVG(edad) FROM ciclista WHERE nomequipo="Banesto"'
        ]);
    }

    public function actionConsulta5b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT AVG(edad) AS "edad media",nomequipo FROM ciclista GROUP BY nomequipo',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['edad media', 'nomequipo'],
                    "titulo" => "consulta 5 con dao",
                    "enunciado" => "La edad media de los ciclistas por cada equipo",
                    "sql" => 'SELECT AVG(edad) FROM ciclista WHERE nomequipo="Banesto"'
        ]);
    }

    public function actionConsulta6b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS "numero de ciclistas",nomequipo FROM ciclista  GROUP BY nomequipo',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['numero de ciclistas', 'nomequipo'],
                    "titulo" => "consulta 6 con dao",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => 'SELECT COUNT(*) AS "numero de ciclistas",c.nomequipo FROM ciclista c GROUP BY c.nomequipo'
        ]);
    }

    public function actionConsulta7b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['numero de puertos'],
                    "titulo" => "consulta 7 con dao",
                    "enunciado" => "El número total de puertos",
                    "sql" => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto'
        ]);
    }

    public function actionConsulta8b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto  WHERE altura>1500',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['numero de puertos'],
                    "titulo" => "consulta 8 con dao",
                    "enunciado" => "El número total de puertos",
                    "sql" => 'SELECT COUNT(*) AS "numero de puertos" FROM puerto  WHERE altura>1500'
        ]);
    }

    public function actionConsulta9b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT nomequipo FROM ciclista  GROUP BY nomequipo HAVING COUNT(*)>4',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "consulta 9 con dao",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql" => 'SELECT nomequipo FROM ciclista  GROUP BY nomequipo HAVING COUNT(*)>4'
        ]);
    }

    public function actionConsulta10b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT nomequipo FROM ciclista  WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING COUNT(*)>4',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "consulta 10 con dao",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" => 'SELECT nomequipo FROM ciclista  WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING COUNT(*)>4'
        ]);
    }

    public function actionConsulta11b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT dorsal, COUNT(dorsal) AS "etapas ganadas" FROM etapa  GROUP BY dorsal',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal', 'etapas ganadas'],
                    "titulo" => "consulta 11 con dao",
                    "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" => 'SELECT dorsal, COUNT(dorsal) AS "etapas ganadas" FROM etapa  GROUP BY dorsal'
        ]);
    }

    public function actionConsulta12b() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT dorsal, COUNT(dorsal) AS "etapas ganadas" FROM etapa  GROUP BY dorsal HAVING COUNT(*)>1',
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal', 'etapas ganadas'],
                    "titulo" => "consulta 12 con dao",
                    "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" => 'SELECT dorsal, COUNT(dorsal) AS "etapas ganadas" FROM etapa  GROUP BY dorsal HAVING COUNT(*)>1'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
